\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{6}
\contentsline {subsection}{\numberline {1.1}Where used?}{6}
\contentsline {subsection}{\numberline {1.2}Characteristics of ES}{6}
\contentsline {subsection}{\numberline {1.3}ES vs. General Purpose System}{6}
\contentsline {section}{\numberline {2}Software Introduction}{8}
\contentsline {subsection}{\numberline {2.1}Real-Time Systems}{8}
\contentsline {subsection}{\numberline {2.2}Time-Triggered Systems}{8}
\contentsline {subsubsection}{\numberline {2.2.1}Informations}{8}
\contentsline {subsection}{\numberline {2.3}Example - Simple periodic time-triggered Scheduler}{8}
\contentsline {subsection}{\numberline {2.4}Example - Time-triggered cyclic executive Scheduler}{8}
\contentsline {subsubsection}{\numberline {2.4.1}Conditions}{8}
\contentsline {subsubsection}{\numberline {2.4.2}Properties}{8}
\contentsline {subsection}{\numberline {2.5}Event triggered Systems}{8}
\contentsline {subsection}{\numberline {2.6}Example - Non-preemtive event triggerd scheduling}{10}
\contentsline {subsubsection}{\numberline {2.6.1}Principle}{10}
\contentsline {subsubsection}{\numberline {2.6.2}Properties}{10}
\contentsline {subsection}{\numberline {2.7}Example - Preemtive event triggered scheduling}{10}
\contentsline {subsubsection}{\numberline {2.7.1}Stack Policy}{10}
\contentsline {subsection}{\numberline {2.8}Petri Nets}{10}
\contentsline {subsection}{\numberline {2.9}Process}{10}
\contentsline {subsubsection}{\numberline {2.9.1}Co-operative Multitasking}{10}
\contentsline {subsubsection}{\numberline {2.9.2}Preemtive Multitasking}{10}
\contentsline {section}{\numberline {3}Real Time Models}{12}
\contentsline {subsection}{\numberline {3.1}Classification}{12}
\contentsline {subsection}{\numberline {3.2}General}{12}
\contentsline {subsection}{\numberline {3.3}Schedule and Timing}{12}
\contentsline {subsection}{\numberline {3.4}Metrics}{12}
\contentsline {section}{\numberline {4}Aperiodic Tasks}{16}
\contentsline {subsection}{\numberline {4.1}FIFO/FCFS - First in, first out/First come, first serve}{16}
\contentsline {subsubsection}{\numberline {4.1.1}Assumptions}{16}
\contentsline {subsubsection}{\numberline {4.1.2}Algorithm}{16}
\contentsline {subsection}{\numberline {4.2}SJF - Shortest Job First}{16}
\contentsline {subsubsection}{\numberline {4.2.1}Assumptions}{16}
\contentsline {subsubsection}{\numberline {4.2.2}Algorithm}{16}
\contentsline {subsubsection}{\numberline {4.2.3}Properties}{16}
\contentsline {subsection}{\numberline {4.3}Shortest remaining time next}{16}
\contentsline {subsubsection}{\numberline {4.3.1}Assumptions}{16}
\contentsline {subsubsection}{\numberline {4.3.2}Algorithm}{16}
\contentsline {subsubsection}{\numberline {4.3.3}Properties}{16}
\contentsline {subsection}{\numberline {4.4}RR - Round Robin}{16}
\contentsline {subsubsection}{\numberline {4.4.1}Assumptions}{16}
\contentsline {subsubsection}{\numberline {4.4.2}Algorithm}{16}
\contentsline {subsection}{\numberline {4.5}EDD - Earliest Deadline Due}{16}
\contentsline {subsubsection}{\numberline {4.5.1}Assumptions}{16}
\contentsline {subsubsection}{\numberline {4.5.2}Algorithm}{16}
\contentsline {subsubsection}{\numberline {4.5.3}Properties}{16}
\contentsline {subsection}{\numberline {4.6}EDF - Earliest Deadline First}{18}
\contentsline {subsubsection}{\numberline {4.6.1}Assumptions}{18}
\contentsline {subsubsection}{\numberline {4.6.2}Algorithm}{18}
\contentsline {subsubsection}{\numberline {4.6.3}Properties}{18}
\contentsline {subsection}{\numberline {4.7}EDF* - Earliest Deadline First}{18}
\contentsline {subsubsection}{\numberline {4.7.1}Assumptions}{18}
\contentsline {subsubsection}{\numberline {4.7.2}Algorithm}{18}
\contentsline {subsubsection}{\numberline {4.7.3}Properties}{18}
\contentsline {subsection}{\numberline {4.8}LDF - Latest Deadline First}{18}
\contentsline {subsubsection}{\numberline {4.8.1}Assumptions}{18}
\contentsline {subsubsection}{\numberline {4.8.2}Algorithm}{18}
\contentsline {subsubsection}{\numberline {4.8.3}Properties}{20}
\contentsline {section}{\numberline {5}Periodic Tasks}{20}
\contentsline {subsection}{\numberline {5.1}Model}{20}
\contentsline {subsection}{\numberline {5.2}RM - Rate Monotonic Scheduling}{22}
\contentsline {subsubsection}{\numberline {5.2.1}Assumptions}{22}
\contentsline {subsubsection}{\numberline {5.2.2}Algorithm}{22}
\contentsline {subsubsection}{\numberline {5.2.3}Properties}{22}
\contentsline {subsubsection}{\numberline {5.2.4}Theorem}{22}
\contentsline {subsection}{\numberline {5.3}DM - Deadline Monotonic Scheduling}{22}
\contentsline {subsubsection}{\numberline {5.3.1}Assumptions}{22}
\contentsline {subsubsection}{\numberline {5.3.2}Algorithm}{22}
\contentsline {subsubsection}{\numberline {5.3.3}Theorem}{22}
\contentsline {subsection}{\numberline {5.4}EDF Scheduling - Earliest Deadline First}{24}
\contentsline {subsubsection}{\numberline {5.4.1}Assumptions}{24}
\contentsline {subsubsection}{\numberline {5.4.2}Algorithm}{24}
\contentsline {subsubsection}{\numberline {5.4.3}Properties}{24}
\contentsline {subsubsection}{\numberline {5.4.4}Theorem}{24}
\contentsline {subsection}{\numberline {5.5}Background Scheduling}{24}
\contentsline {subsection}{\numberline {5.6}RM - Polling Server}{24}
\contentsline {subsubsection}{\numberline {5.6.1}Assumption}{24}
\contentsline {subsubsection}{\numberline {5.6.2}Algorithm}{24}
\contentsline {subsubsection}{\numberline {5.6.3}Properties}{24}
\contentsline {subsubsection}{\numberline {5.6.4}Theorem}{24}
\contentsline {subsection}{\numberline {5.7}EDF - Total Bandwidth Server}{26}
\contentsline {subsubsection}{\numberline {5.7.1}Assumptions}{26}
\contentsline {subsubsection}{\numberline {5.7.2}Algorithm}{26}
\contentsline {subsubsection}{\numberline {5.7.3}Theorem}{26}
\contentsline {section}{\numberline {6}Resource sharing}{28}
\contentsline {subsection}{\numberline {6.1}Terms}{28}
\contentsline {subsection}{\numberline {6.2}Solutions to Resource Sharing}{28}
\contentsline {subsection}{\numberline {6.3}Priority Inversion}{28}
\contentsline {subsubsection}{\numberline {6.3.1}Problem}{28}
\contentsline {subsubsection}{\numberline {6.3.2}Solution - Disallow preemption}{28}
\contentsline {subsubsection}{\numberline {6.3.3}Solution - Priority Inheritance Protocol (PIP)}{28}
\contentsline {section}{\numberline {7}RTOS: Real-Time OS}{30}
\contentsline {subsection}{\numberline {7.1}Desktop OS not suited}{30}
\contentsline {subsection}{\numberline {7.2}Properties of RTOS/Embedded OS}{30}
\contentsline {subsection}{\numberline {7.3}Requirements}{30}
\contentsline {subsubsection}{\numberline {7.3.1}Timing Behavior}{30}
\contentsline {subsubsection}{\numberline {7.3.2}Timing}{30}
\contentsline {subsubsection}{\numberline {7.3.3}Fast}{30}
\contentsline {subsection}{\numberline {7.4}Functionality of RTOS-Kernels}{30}
\contentsline {subsection}{\numberline {7.5}Context-Switch}{30}
\contentsline {subsection}{\numberline {7.6}Process states}{30}
\contentsline {subsection}{\numberline {7.7}Threads}{32}
\contentsline {subsubsection}{\numberline {7.7.1}Process Management}{32}
\contentsline {subsection}{\numberline {7.8}Synchronous Communication}{32}
\contentsline {subsection}{\numberline {7.9}Asynchronous Communication}{32}
\contentsline {subsection}{\numberline {7.10}Class of Kernels}{32}
\contentsline {subsubsection}{\numberline {7.10.1}Class 1 - Fast Proprietary Kernels}{32}
\contentsline {subsubsection}{\numberline {7.10.2}Class 2 - Extensions to Standard OSs}{32}
\contentsline {subsubsection}{\numberline {7.10.3}Class 3 - Research Systems}{32}
\contentsline {section}{\numberline {8}System Components}{34}
\contentsline {subsection}{\numberline {8.1}General Purpose Processors}{34}
\contentsline {subsubsection}{\numberline {8.1.1}Properties}{34}
\contentsline {subsubsection}{\numberline {8.1.2}Multicore Processors}{34}
\contentsline {subsection}{\numberline {8.2}System Specialization}{34}
\contentsline {subsubsection}{\numberline {8.2.1}Flexibility}{34}
\contentsline {subsubsection}{\numberline {8.2.2}Examples}{34}
\contentsline {subsubsection}{\numberline {8.2.3}Microcontroller}{34}
\contentsline {subsection}{\numberline {8.3}Digital Signal Processors/VLIW}{34}
\contentsline {subsubsection}{\numberline {8.3.1}General}{34}
\contentsline {subsubsection}{\numberline {8.3.2}Very Long Instruction Word - VLIW}{34}
\contentsline {subsection}{\numberline {8.4}Programmable Hardware - FPGA}{34}
\contentsline {subsection}{\numberline {8.5}Application Specific Circuits - ASICs}{36}
\contentsline {subsection}{\numberline {8.6}System on a Chip}{36}
\contentsline {section}{\numberline {9}Communication}{38}
\contentsline {subsection}{\numberline {9.1}Requirements}{38}
\contentsline {subsection}{\numberline {9.2}Classification}{38}
\contentsline {subsection}{\numberline {9.3}Random Access}{38}
\contentsline {subsubsection}{\numberline {9.3.1}Fully randomized}{38}
\contentsline {subsubsection}{\numberline {9.3.2}Slotted random access}{38}
\contentsline {subsection}{\numberline {9.4}Time Division Multiple Access - TDMA}{38}
\contentsline {subsection}{\numberline {9.5}Carrier Sense Multiple Access / Collision Detection -- CSMA/CD}{38}
\contentsline {subsection}{\numberline {9.6}Token Protocols}{38}
\contentsline {subsection}{\numberline {9.7}Token Ring}{40}
\contentsline {subsection}{\numberline {9.8}Carrier Sense Multiple Access / Collision Avoidance - CSMA/CA - Flexible TDMA (FTDMA)}{40}
\contentsline {subsection}{\numberline {9.9}Carrier Sense Multiple Access / Collision Resolution -- CSMA/CR}{40}
\contentsline {subsection}{\numberline {9.10}FlexRay}{40}
\contentsline {subsubsection}{\numberline {9.10.1}General}{40}
\contentsline {subsubsection}{\numberline {9.10.2}Principle}{40}
\contentsline {subsubsection}{\numberline {9.10.3}TDMA}{40}
\contentsline {subsubsection}{\numberline {9.10.4}Flexible TDMA}{40}
\contentsline {subsection}{\numberline {9.11}Bluetooth}{40}
\contentsline {subsubsection}{\numberline {9.11.1}Design Goals}{40}
\contentsline {subsubsection}{\numberline {9.11.2}Technical Data}{40}
\contentsline {subsubsection}{\numberline {9.11.3}Frequency Hopping}{40}
\contentsline {subsubsection}{\numberline {9.11.4}Network Topologies}{42}
\contentsline {subsubsection}{\numberline {9.11.5}Packet Format}{42}
\contentsline {subsubsection}{\numberline {9.11.6}Addressing}{42}
\contentsline {subsubsection}{\numberline {9.11.7}Connection Types}{42}
\contentsline {subsubsection}{\numberline {9.11.8}Frequency Hopping Time Multiplex}{42}
\contentsline {subsubsection}{\numberline {9.11.9}Multi-Slot Communication}{42}
\contentsline {subsubsection}{\numberline {9.11.10}Modes of Operation}{44}
\contentsline {subsubsection}{\numberline {9.11.11}States in Connection Mode}{44}
\contentsline {subsubsection}{\numberline {9.11.12}Synchronization in Connection Mode}{44}
\contentsline {subsubsection}{\numberline {9.11.13}Page Mode}{44}
\contentsline {subsubsection}{\numberline {9.11.14}Protocol Hierarchy}{44}
\contentsline {section}{\numberline {10}Low Power Design}{48}
\contentsline {subsection}{\numberline {10.1}General}{48}
\contentsline {subsection}{\numberline {10.2}Power and Energy}{48}
\contentsline {subsubsection}{\numberline {10.2.1}Low power consumption}{48}
\contentsline {subsubsection}{\numberline {10.2.2}Low energy consumption}{48}
\contentsline {subsubsection}{\numberline {10.2.3}Power Consumption of CMOS Gate}{48}
\contentsline {subsubsection}{\numberline {10.2.4}Types of Power Consumption/ Consumption of CMOS Processors}{48}
\contentsline {subsubsection}{\numberline {10.2.5}Dynamic Voltage Scaling - DVS}{48}
\contentsline {subsubsection}{\numberline {10.2.6}Power Supply Gating}{48}
\contentsline {subsection}{\numberline {10.3}Parallelism}{50}
\contentsline {subsection}{\numberline {10.4}Pipelining}{50}
\contentsline {subsection}{\numberline {10.5}VLIW}{50}
\contentsline {subsection}{\numberline {10.6}Dynamic Voltage Scaling}{50}
\contentsline {subsection}{\numberline {10.7}YDS Algorithm for Offline Scheduling}{50}
\contentsline {subsection}{\numberline {10.8}DVS - Online Scheduling on One Processor}{50}
\contentsline {subsection}{\numberline {10.9}Dynamic Power Management (DPM)}{50}
\contentsline {subsection}{\numberline {10.10}Procrastination Schedule}{52}
\contentsline {section}{\numberline {11}Architecture Design}{54}
\contentsline {subsection}{\numberline {11.1}Task Graph/Dependence Graph/Data Flow Graph}{54}
\contentsline {subsection}{\numberline {11.2}Control-Data Flow Graph (CDFG)}{54}
\contentsline {subsubsection}{\numberline {11.2.1}Control Flow Graph (statement transitions)}{54}
\contentsline {subsubsection}{\numberline {11.2.2}Data Flow Graph (statement dependence)}{54}
\contentsline {subsection}{\numberline {11.3}Sequence Graph (SG)}{54}
\contentsline {subsubsection}{\numberline {11.3.1}Example}{56}
\contentsline {subsection}{\numberline {11.4}Resource Graph}{56}
\contentsline {subsection}{\numberline {11.5}Marked Graphs (MG)}{56}
\contentsline {subsubsection}{\numberline {11.5.1}Implementation as a synchronous digital circuit}{56}
\contentsline {subsubsection}{\numberline {11.5.2}Implementation as a self-timed asynchronous circuit}{56}
\contentsline {subsubsection}{\numberline {11.5.3}Implementation in software (static scheduling)}{58}
\contentsline {subsubsection}{\numberline {11.5.4}Implementation in software (dynamic scheduling)}{58}
\contentsline {section}{\numberline {12}Architecture Synthesis}{60}
\contentsline {subsection}{\numberline {12.1}General}{60}
\contentsline {subsection}{\numberline {12.2}Model}{60}
\contentsline {subsection}{\numberline {12.3}Classification}{60}
\contentsline {subsection}{\numberline {12.4}Allocation, Binding, Scheduling, Latency, Mobility}{60}
\contentsline {subsection}{\numberline {12.5}Multiobjective Optimization}{60}
\contentsline {subsection}{\numberline {12.6}Pareto}{60}
\contentsline {subsection}{\numberline {12.7}Scheduling without Resource Constraints}{62}
\contentsline {subsubsection}{\numberline {12.7.1}Problem Definition}{62}
\contentsline {subsubsection}{\numberline {12.7.2}As Soon as Possible (ASAP) - Algorithm}{62}
\contentsline {subsubsection}{\numberline {12.7.3}As Late As Possible (ALAP) - Algorithm}{62}
\contentsline {subsection}{\numberline {12.8}Scheduling with Timing Constraints}{62}
\contentsline {subsubsection}{\numberline {12.8.1}Examples of Timing Constraints}{62}
\contentsline {subsubsection}{\numberline {12.8.2}Conversion Rules}{62}
\contentsline {subsubsection}{\numberline {12.8.3}Weighted Constraint Graph}{62}
\contentsline {subsubsection}{\numberline {12.8.4}Bellman-Ford}{64}
\contentsline {subsection}{\numberline {12.9}Scheduling with resource constraints}{64}
\contentsline {subsubsection}{\numberline {12.9.1}List Scheduling}{64}
\contentsline {subsubsection}{\numberline {12.9.2}Integer Linear Programming}{64}
\contentsline {subsubsection}{\numberline {12.9.3}Iterative Algorithms}{66}
\contentsline {subsubsection}{\numberline {12.9.4}Dynamic Voltage Scaling}{68}
\contentsline {section}{\numberline {13}Labs}{70}
\contentsline {subsection}{\numberline {13.1}General}{70}
\contentsline {subsection}{\numberline {13.2}Terminal/minicom}{70}
\contentsline {subsection}{\numberline {13.3}Running Time}{70}
\contentsline {subsection}{\numberline {13.4}Looping}{70}
\contentsline {subsection}{\numberline {13.5}LED on}{70}
\contentsline {subsection}{\numberline {13.6}Battery}{70}
\contentsline {subsubsection}{\numberline {13.6.1}Theorem}{70}
\contentsline {subsubsection}{\numberline {13.6.2}Code}{70}
\contentsline {subsection}{\numberline {13.7}Interrupts}{72}
\contentsline {subsection}{\numberline {13.8}OS}{72}
\contentsline {subsubsection}{\numberline {13.8.1}General}{72}
\contentsline {subsubsection}{\numberline {13.8.2}LED}{72}
\contentsline {subsubsection}{\numberline {13.8.3}Timer}{72}
\contentsline {subsubsection}{\numberline {13.8.4}Terminal}{74}
\contentsline {subsubsection}{\numberline {13.8.5}Threads}{74}
\contentsline {subsubsection}{\numberline {13.8.6}Events}{76}
\contentsline {section}{\numberline {14}Bluetooth}{76}
\contentsline {subsection}{\numberline {14.1}Overview}{76}
\contentsline {subsection}{\numberline {14.2}Bluetooth Header}{76}
\contentsline {section}{\numberline {15}Threads}{76}
\contentsline {subsection}{\numberline {15.1}Creating a thread}{76}
\contentsline {subsection}{\numberline {15.2}Terminating a thread}{76}
\contentsline {subsection}{\numberline {15.3}Sleep}{76}
\contentsline {subsection}{\numberline {15.4}Co-Operative Scheduling}{78}
\contentsline {section}{\numberline {16}Appendix}{80}
\contentsline {subsection}{\numberline {16.1}Derivative}{80}
